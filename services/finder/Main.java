import java.awt.image.*;
import java.io.*;
import java.util.*;
import javax.imageio.*;

public class Main {

    public static String getFileNameWithoutExtension(String fileName) {

        int lastDotPos = fileName.lastIndexOf('.');
        if (lastDotPos > 0) {
            return fileName.substring(0, lastDotPos);
        }
        return "";

    }

    public static File[] getFiles(String dir) {

        return new File(dir).listFiles();

    }

    public static int[] getAllRGB(BufferedImage img) {

        int w = img.getWidth();
        int h = img.getHeight();
        return img.getRGB(
            0,    // the starting X coordinate
            0,    // the starting Y coordinate
            w,    // width of region
            h,    // height of region
            null, // if not null, the rgb pixels are written here
            0,    // offset into the rgbArray
            w     // scanline stride for the rgbArray
        );

    }

    public static SimilarImage findSimilarImage(BufferedImage canonImage, String variantsDir, int acceptErrPerc) {

        int canonWidth = canonImage.getWidth();
        int canonHeight = canonImage.getHeight();

        int[] canonRGB = getAllRGB(canonImage);

        File[] variants = getFiles(variantsDir);
        if (variants == null) {
            return null;
        }

        SimilarImage similarImage = new SimilarImage();

        int maxSimilarPxs = 0;

        variantsloop:
        for (File variant : variants) {
            try {
                BufferedImage variantImage = ImageIO.read(variant);

                int variantWidth = variantImage.getWidth();
                int variantHeight = variantImage.getHeight();
                if (variantWidth != canonWidth || variantHeight != canonHeight) {
                    continue;
                }

                int[] variantImageRGB = getAllRGB(variantImage);

                int similarPxs = 0;
                int differentPxs = 0;
                for (int i = 0; i < canonRGB.length; ++i) {
                    if (variantImageRGB[i] == canonRGB[i]) {
                        ++similarPxs;
                    } else {
                        ++differentPxs;
                    }

                    if ((double)differentPxs / variantImageRGB.length * 100.0 > acceptErrPerc) {
                        continue variantsloop;
                    }
                }

                if (similarPxs > maxSimilarPxs) {
                    similarImage.name = variant.getName();
                    maxSimilarPxs = similarPxs;
                }

                if (similarPxs == canonRGB.length) {
                    break;
                }
            } catch (Exception ex) {
                // ignored
            }
        }

        similarImage.confidence = (int)((double)maxSimilarPxs / canonRGB.length * 100.0);

        return similarImage;

    }

    public static void main(String[] args) throws IOException {

        if (args.length < 2) {
            System.err.println("Not enough arguments. Usage: java Main %canonimagepath% %variantsdir% %accepterrperc%");
            return;
        }

        String canonImagePath = args[0];
        String variantsDir = args[1];
        int acceptErrPerc = Integer.parseInt(args[2]);

        BufferedImage canonImage = ImageIO.read(new File(canonImagePath));
        SimilarImage similarImage = findSimilarImage(canonImage, variantsDir, acceptErrPerc);
        if (similarImage != null) {
            String coords = getFileNameWithoutExtension(similarImage.name).replace("_", ",");
            System.out.println(coords + " " + similarImage.confidence);
        }

    }
}
