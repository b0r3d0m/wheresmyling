import java.awt.image.*;
import java.io.*;
import java.net.*;
import javax.imageio.*;

public class Main {

    public static String getExtenstion(String fileName) {

        int lastDotPos = fileName.lastIndexOf('.');
        if (lastDotPos > 0) {
            return fileName.substring(lastDotPos + 1);
        }
        return "";

    }

    public static void downloadImage(String url, String to, String format) throws IOException {

        BufferedImage image = ImageIO.read(new URL(url));
        ImageIO.write(image, format, new File(to));

    }

    public static void downloadImage(String url, String to) throws IOException {

        String extension = getExtenstion(to);
        if (extension.isEmpty()) {
            throw new RuntimeException("Unable to determine format name");
        }
        downloadImage(url, to, extension);

    }

    public static String pathJoin(String path1, String path2) {

        return new File(path1, path2).toString();

    }

    public static void mkDirs(String path) {

        new File(path).mkdirs();

    }

    public static void downloadMapTile(int x, int y, String to) throws IOException {

        String url = String.format("http://www.odditown.com:8080/haven/tiles/live/9/%d_%d.png", x, y); // 9 for max. zoom
        downloadImage(url, to);

    }

    public static void downloadOddiTownMap(String outputDirectory) {

        mkDirs(outputDirectory);

        // Hafen's map is a 250x250 grid
        for (int x = -120; x < 130; ++x) {
            for (int y = -120; y < 130; ++y) {
                try {
                    String saveTo = pathJoin(outputDirectory, String.format("%d_%d.png", x, y));
                    downloadMapTile(x, y, saveTo);
                } catch (Exception ex) {
                    // ignored
                }
            }
        }

    }

    public static void main(String[] args) {

        if (args.length < 1) {
            System.err.println("Not enough arguments. You should pass an output directory as an argument");
            return;
        }

        String outputDirectory = args[0];
        downloadOddiTownMap(outputDirectory);

    }
}
