// Load additional modules
var exec = require('child_process').exec;
var fs = require('fs');
var path = require('path');
var uuid = require('node-uuid');
var util = require('util');

// Constants
const finderDir = path.resolve(__dirname, '../services/finder');
const mapsDir = path.resolve(__dirname, '../services/downloader/map');
const acceptErrPerc = 10;

// Read config
var nconf = require('nconf');
nconf.file({ file: 'config.json' });

// Configure Express
var express = require('express');
var app = express();

// Setup views rendering
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// Setup "multipart/form-data" parser
var busboy = require('connect-busboy');
app.use(busboy({ immediate: true }));

// Define routes

app.get('/', function(req, res) {
  res.render('index');
});

app.get('/notfound', function(req, res) {
  res.render('notfound');
});

app.get('/result', function(req, res) {
  var mapTileFileName = util.format('%d_%d.png', req.query.x, req.query.y);
  fs.readFile(path.join(mapsDir, mapTileFileName), function(err, data) {
    res.render('result', {
      x: req.query.x,
      y: req.query.y,
      confidence: req.query.confidence,
      tile: 'data:image/png;base64,' + getBase64(data)
    });
  });
});

app.post('/find', function(req, res) {
  req.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {

    // We can't just pass the file's binary data as an argument
    // because of the "Command line is too long" error

    var uploadedImagePath = path.resolve(__dirname, uuid.v4() + '.png');
    saveFile(file, uploadedImagePath, function() {
      exec(util.format('java Main "%s" "%s" %d', uploadedImagePath, mapsDir, acceptErrPerc), {cwd: finderDir}, function(error, stdout, stderr) {
        if (!stdout) {
          res.redirect('/notfound');
        } else {
          // The output format of the finder is "x_y confidence"
          var tokens = stdout.split(' ');

          var coords = tokens[0].split(',');
          var x = parseInt(coords[0]);
          var y = parseInt(coords[1]);

          var confidence = parseInt(tokens[1]);

          res.redirect(util.format('/result?x=%d&y=%d&confidence=%d', x, y, confidence));
        }
        removeFile(uploadedImagePath);
      });
    });
  });
});

// Helper functions

function getBase64(data) {
  return new Buffer(data).toString('base64');
}

function saveFile(file, fileName, cb) {
  var fstream = fs.createWriteStream(fileName);
  file.pipe(fstream);
  fstream.on('close', cb);
}

function removeFile(fileName, cb) {
  fs.unlink(fileName, cb);
}

// Start server

var port = nconf.get('general').port;
var server = app.listen(port, function() {
  console.log("App is started at PORT " + port);
});

// Let's increase default Express timeout
// because we can search for the coords much longer than 2 mins
server.timeout = 30 * 60 * 1000; // 60 mins
